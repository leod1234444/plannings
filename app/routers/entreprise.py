#System imports

#Libs imports
from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder

#Local imports
from internal.bdd import db

router = APIRouter()

# bellow is a second way to instantiate a list of data. Here we instantiate them as instances of Car

@router.get("/entreprises")
async def get_all_cars():
    aql = "For e in Entreprises RETURN e"
    queryResult = db.AQLQuery(aql, rawResults=True, batchSize=100, json_encoder = None)
    print(queryResult)
    return queryResult

@router.get("/cars")
async def get_all_users():
    return ""

