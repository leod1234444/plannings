#System imports

#Libs imports
from fastapi import FastAPI, Response, status

#Local imports
from routers import entreprise, users
from internal import auth
from internal.bdd import db
from internal.bdd import innit

try:
    db["Users"]
except KeyError:
    innit(db)





app = FastAPI()

app.include_router(auth.router, tags=["Authentification"])
app.include_router(entreprise.router, tags=["Entreprises"])
app.include_router(users.router, tags=["User"])
